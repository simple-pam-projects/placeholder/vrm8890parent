
# VRM8890

Project based on https://mocloud.atlassian.net/browse/VRM-8890

## Project Structure

The VRM8890 project has been set up as a multi-module project with the [vrm8890Parent](https://bitbucket.org/motabilityoperations/vrm8890parent/src/master/) project being the "parent" module holding maven variables for versions, dependecy information and a common set of `distributionElement` attributes.

Overall project structure is as follows:

```
vrm8890Parent
   +-- vrm8890DataModel     : Common data model, deployed to maven repo
   +-- vrm8890Rules         : Rules applicable to VRM8890, deployed to maven repo
   +-- vrm8890RuleSupport   : Event listeners and supporting elements, deployed to maven repo
   +-- vrm8890Client        : Sample Java based KIE Client, NOT deployed to maven repo
                              Also used for "integration" testing.
```

Individual projects:

* **Data Model** The [vrm8890DataModel](https://bitbucket.org/motabilityoperations/vrm8890datamodel) project is used to model the Entities (Java POJO objects) that are used for interacting with the Decision Engine. It should cover both *input* as well as *output* entities.
* **Rules** The [vrm8890Rules](https://bitbucket.org/motabilityoperations/vrm8890rules) project captures the relevant business rules. It is also home of basic unit testing. It should contain the DRL file (or files) and Java code or other artefacts that directly support the rules. Occasionally some rules may call for significant algorithmical (or procedural) processing that is best implemented in regular Java. Classes that implement such processing should be placed in this project as well.
* **RuleSupport** The [vrm8890RuleSupport](https://bitbucket.org/motabilityoperations/vrm8890rulesupport) project is used for classes and methods that could augment the main Rules project or offer additional supportive Entities not belonging in the main business rules domain. Currently sample event listeners are included.

## Building

Building the project requires the standard maven toolchain. However for a KJAR to be produced and deposited to a maven repository so that it can later be used by KIE Server the `deploy` target should be used.

Invoking maven with `deploy` target will build the source code and perform the unit tests

```
mvn clean deploy
```

Unit tests that complete successfully would result in an output similar to the following:

```
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running scot.dagda.vrm8890Rules.RuleTest
2020-10-01 08:43:51,666 INFO  [org.drools.compiler.kie.builder.impl.ClasspathKieProject] (main) Found kmodule: file:/home/erouvas/main/rh/pelatis/motability/vrm8890Rules/target/classes/META-INF/kmodule.xml
2020-10-01 08:43:51,835 WARN  [org.drools.compiler.kie.builder.impl.ClasspathKieProject] (main) Unable to find pom.properties in /home/erouvas/main/rh/pelatis/motability/vrm8890Rules/target/classes
2020-10-01 08:43:51,841 INFO  [org.drools.compiler.kie.builder.impl.ClasspathKieProject] (main) Recursed up folders, found and used pom.xml /home/erouvas/main/rh/pelatis/motability/vrm8890Rules/pom.xml
2020-10-01 08:43:52,395 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Creating kieBase
2020-10-01 08:43:52,457 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) There should be rules:
2020-10-01 08:43:52,457 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) kp [Package name=scot.dagda.vrm8890Rules] rule GET_SalesRecommendation
2020-10-01 08:43:52,458 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) kp [Package name=scot.dagda.vrm8890Rules] rule GET_Audit
2020-10-01 08:43:52,458 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) kp [Package name=scot.dagda.vrm8890Rules] rule AC1_CAR
2020-10-01 08:43:52,458 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) kp [Package name=scot.dagda.vrm8890Rules] rule AC2_WAV
2020-10-01 08:43:52,458 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) kp [Package name=scot.dagda.vrm8890Rules] rule AC3_LCV
2020-10-01 08:43:52,458 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Creating kieSession
2020-10-01 08:43:52,479 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Now running data
2020-10-01 08:43:52,504 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Final checks
2020-10-01 08:43:52,504 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Check Sales Recommendation for v1car
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) SalesRecommendation{vehicleId=v1car, salesPlatform=mfldirect, salesChannel=Pre-Expiry}
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Check Audit for v1car
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Audit{vehicleId=v1car, ruleName=AC1_CAR}
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Check Sales Recommendation for v2wav
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) SalesRecommendation{vehicleId=v2wav, salesPlatform=N/A, salesChannel=N/A}
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Check Audit for v2wav
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Audit{vehicleId=v2wav, ruleName=AC2_WAV}
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Check Sales Recommendation for v3lcv
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) SalesRecommendation{vehicleId=v3lcv, salesPlatform=N/A, salesChannel=N/A}
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Check Audit for v3lcv
2020-10-01 08:43:52,505 INFO  [scot.dagda.vrm8890Rules.RuleTest] (main) Audit{vehicleId=v3lcv, ruleName=AC3_LCV}
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.994 s - in scot.dagda.vrm8890Rules.RuleTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
[INFO]
```


## Invoking the Rules

With the `vrm8890Rules` KJAR deposited in the Maven repo, the KIE Server can be instructed to create a KIE Container with it. The [vrm8890Client](https://bitbucket.org/motabilityoperations/vrm8890client) implements a sample Java client that would invoke the KIE Container.

> The current `vrm8890Client` implementation has hardcoded parameters for the KIE Server and the KIE Container to invoke. These parameters should eventually be externalized.


## Initial project creation

Initial project structure was based on Maven archetypes. [KIE Archetypes](https://mvnrepository.com/artifact/org.kie/kie-archetypes?repo=redhat-ga) were used for Rules based projects and regular maven archetyped for the rest. Example:

```
mvn archetype:generate \
    -DgroupId=scot.dagda.vrm8890Rules -DartifactId=vrm8890Rules \
    -DarchetypeGroupId=org.kie -DarchetypeArtifactId=kie-drools-archetype -DarchetypeVersion=7.39.Final-redhat-00007
    -DpomEclipseCompatible=true -B

mvn archetype:generate \
    -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-quickstart \
    -DarchetypeVersion=1.4 \
    -DgroupId=scot.dagda.vrm8890Client -DartifactId=vrm8890Client -Dversion=1.0-SNAPSHOT -B
```

Basic Decision Manager dependecnies when subsequently modified to use BOMs as per:

* Mapping between Red Hat Decision Manager and the Maven library version : https://access.redhat.com/solutions/3363991
